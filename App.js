/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import {
   Text,
   View,
   Image
 } from 'react-native';
 
 import 'react-native-gesture-handler';
 
 import { StyleSheet } from 'react-native';
 import { NavigationContainer } from '@react-navigation/native';
 import { createStackNavigator } from '@react-navigation/stack';
 
 import Intro from './src/screens/Intro';
 import Login from './src/screens/Login';
 import Register from './src/screens/Register';
 import Recuperar from './src/screens/Recuperar';
 import Confirmation from './src/screens/Confirmation';
 import NewPass from './src/screens/NewPass';

 
 const Stack = createStackNavigator();
 
 const App: () => Node = (props) => {
   const{navigation} = props;
   return (
     <NavigationContainer>
         <Stack.Navigator>
           <Stack.Screen
            options={{headerShown:false}}
             name="Intro"
             component={Intro}
           />

          <Stack.Screen
            options={{headerShown:false}}
             name="Login"
             component={Login}
           />

          <Stack.Screen
            options={{headerShown:false}}
             name="Register"
             component={Register}
           />
            <Stack.Screen
            options={{headerShown:false}}
             name="Recuperar"
             component={Recuperar}
           />
             <Stack.Screen
            options={{headerShown:false}}
             name="Confirmation"
             component={Confirmation}
           />
            <Stack.Screen
            options={{headerShown:false}}
             name="NewPass"
             component={NewPass}
           />
           
         </Stack.Navigator>
       </NavigationContainer>
   );
 };
 
 
 
 export default App;
 