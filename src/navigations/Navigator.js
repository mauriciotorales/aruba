import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Register from '../screens/Register'




const navigator = createStackNavigator(
    {
        Intro:{screen:Intro},

        Register:{screen:Register},
        Login:{screen:Login},
 },)

export default createAppContainer(navigator);