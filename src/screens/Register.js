/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import { SvgUri } from 'react-native-svg';
 import SwipeUpDown from 'react-native-swipe-up-down';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   Image, TextInput
 } from 'react-native';
 

 import CheckBox from '@react-native-community/checkbox';

 
 import { NavigationContainer } from '@react-navigation/native';
 import { createNativeStackNavigator } from '@react-navigation/native-stack';
 
 const Stack = createNativeStackNavigator();

 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 
 
 const Register: () => Node = ({ navigation }) => {

 
 
   return (
     
    <View
        style={{
          backgroundColor: "#FFF",
          height: "100%",
          alignItems: "center",
          paddingTop: 40,
        }}
      >
        <Image
          source={require("../images/register-img.png")}
          style={{ width: 150, height: 150 }}
        />
        <View
          style={{
            backgroundColor: "#FFF",
            paddingLeft: 20,
            paddingRight: 20,
            width: "100%",
          }}
        >
          <Text
            style={{
              fontSize: 30,
              color: "#284C67",
              marginTop: 30,
            }}
          >
            Registro
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
              borderColor: "#00716F",
              borderRadius: 23,
              width: "100%",
            }}
          >
           <SvgUri
                width="48%"
                height="50px"
                uri="https://www.softworkspy.com/facebook.svg"
              />
            <SvgUri
                width="48%"
                height="50px"
                uri="https://www.softworkspy.com/google.svg"
                style={{
                  marginLeft:"4%"
              }}
              />
          </View>
          <Text
            style={{
              fontSize: 12,
              textAlign:"center",
              color: "#5E798D",
              marginTop: 20,
            }}
          >
            O, ingresa con tu e-mail
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
              borderColor: "#00716F",
              borderRadius: 23,
              width: "100%",
            }}
          >
            <Image
              source={require("../images/envelopeIcon.png")}
              style={{ width: 30, height: 30 }}
            />
            <Text
              style={{
                fontSize: 14,
                color: "#93A5B3",
                marginLeft: 10,
              }}
            >
              E-mail
            </Text>
          </View>
          <View style={{}}>
            <TextInput
              style={{
                borderBottomWidth: 1,
                borderColor: "#F4F6F7",
                width: "88%",
                position: "relative",
                marginLeft: 40,
                marginTop:-20
              }}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
              borderColor: "#00716F",
              borderRadius: 23,
              width: "100%",
            }}
          >
            <Image
              source={require("../images/lockIcon.png")}
              style={{ width: 30, height: 30 }}
            />
            <Text
              style={{
                fontSize: 14,
                color: "#93A5B3",
                marginLeft: 10,
              }}
            >
              Contraseña
            </Text>
          </View>
          <View style={{}}>
            <TextInput
              style={{
                borderBottomWidth: 1,
                borderColor: "#F4F6F7",
                width: "88%",
                position: "relative",
                marginLeft: 40,
                marginTop:-20
              }}
            />
          </View>
          <View
            style={{ flexDirection: "row",marginTop:20 }}
          >
            <CheckBox
                style={{
                  borderRadius:10,
                  borderColor:'#93A5B3',
                  marginTop:-8
                }}
            />
            <Text
            style={{
              fontSize: 12,
              color: "#5E798D",
              marginLeft: 10,
            }}
          >
            Acepto las {" "}
            <Text
             onPress={() => navigation.navigate("Login")}
              style={{
                fontSize: 12,
                color: "#284C67",
                textDecorationLine: "underline",
              }}
            >
             Políticas y términos de uso
            </Text>
          </Text>
          </View>
          <View
            style={{
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              marginTop: 30,
              backgroundColor: "#0EEFE9",
              padding: 12,
              borderRadius: 12,
            }}
          >
            <Text
             
              style={{
                color: "#284C67",

              }}
            >
             Registrarme
            </Text>
          </View>

          <Text
            style={{
              fontSize: 12,
              textAlign:"center",
              color: "#5E798D",
              marginTop: 30,
            }}
          >
            Ya tienes una cuenta?{" "}
            <Text
             onPress={() => navigation.navigate("Login")}
              style={{
                fontSize: 12,

                color: "#284C67",
                textDecorationLine: "underline",
              }}
            >
              Iniciar sesión aquí
            </Text>
          </Text>
        </View>

        <Text style={{ margin: 20 }} onPress={() => this.swipeUpDownRef.showFull()}>
          Tap to open panel
        </Text>
        <SwipeUpDown
          hasRef={ref => (this.swipeUpDownRef = ref)}
          itemMini={
            <View style={{ alignItems: 'center' }}>
              <Text>This is the mini view, swipe up!</Text>
            </View>
          }
          itemFull={
            <View >
              <Text >
                Swipe down to close
              </Text>
            </View>
          }
          onShowMini={() => console.log('mini')}
          onShowFull={() => console.log('full')}
          disablePressToShow={false}
          style={{ backgroundColor: '#ccc' }}
         
        />
      </View>
 
           
   );
 };
 
 
 
 export default Register;
 