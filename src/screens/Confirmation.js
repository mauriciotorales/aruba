/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import { SvgUri } from 'react-native-svg';
 import {
   StyleSheet,
   Text,
   View,
   Image,
   TextInput
 } from 'react-native';

 
 
 
 const App: () => Node = ({ navigation }) => {

   return (
     
    <View
    style={{
      backgroundColor: "#FFF",
      height: "100%",
      alignItems: "center",
      paddingTop:50
    }}
    >
      <Image
        source={require("../images/close.png")}
          style={{
            width: 50,
            height: 50,
            position:"absolute",
            left:20,
            top:20,
          }}
        />
    <Image
      source={require("../images/mail.png")}
      style={{ width: 200, height: 200 }}
    />
    <View
      style={{
        backgroundColor: "#FFF",
        paddingLeft: 20,
        paddingRight: 20,
        width:'100%'
      }}
    >
      <Text
        style={{
          fontSize: 30,
          color:'#284C67',
          marginTop:40
        }}
      >
        Código de recuperación
        
      </Text>
      <Text
                style={{
                    fontSize:14,
                    textAlign:"left",
                    color:'#5E798D',
                }}
                >
                Te enviamos el código de recuperación a
juana@mail.com
            </Text>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 20,
          borderColor: "#00716F",
          borderRadius: 23,
          width:'100%'
        }}
      >
        <Image
        source={require("../images/lockIcon.png")}
        style={{ width: 30, height: 30 }}
        />
         <Text
            style={{
            fontSize: 14,
            color:'#93A5B3',
            marginLeft:10
            }}
        >
           Escríbe el código recibido
        </Text>
     </View>
     <View style={{

     }}>
        <TextInput
           style={{
                borderBottomWidth:1,
                borderColor:'#F4F6F7',
                width:'88%',
                position:'relative',
                marginLeft:40
           }}
        />
     </View>
     
            <View style={{
                width:'100%',
                alignItems:"center",
                justifyContent:"center",
                marginTop:50,
                backgroundColor:"#0EEFE9",
                padding:12,
                borderRadius:12
                }}>
                    <Text
                     onPress={()=>navigation.navigate('NewPass')}
                    style={{
                        color:"#284C67",

                    }}>Confirmar código</Text>
            </View>
    </View>
  </View>
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
   },
   highlight: {
   },
 });
 
 export default App;
 