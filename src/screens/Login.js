/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import { SvgUri } from 'react-native-svg';
 import {
   StyleSheet,
   Text,
   View,
   Image,
   TextInput
 } from 'react-native';

 
 
 
 const App: () => Node = ({ navigation }) => {

   return (
    <View
    style={{
      backgroundColor: "#FFF",
      height: "100%",
      alignItems: "center",
      paddingTop:50
    }}
  >
    <Image
      source={require("../images/img-login.png")}
      style={{ width: 150, height: 150 }}
    />
    <View
      style={{
        backgroundColor: "#FFF",
        paddingLeft: 20,
        paddingRight: 20,
        width:'100%'
      }}
    >
      <Text
        style={{
          fontSize: 30,
          color:'#284C67',
          marginTop:40
        }}
      >
        Iniciar sesión
      </Text>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 20,
          borderColor: "#00716F",
          borderRadius: 23,
          width:'100%'
        }}
      >
        <Image
        source={require("../images/envelopeIcon.png")}
        style={{ width: 30, height: 30 }}
        />
         <Text
            style={{
            fontSize: 14,
            color:'#93A5B3',
            marginLeft:10
            }}
        >
           E-mail
        </Text>
     </View>
     <View style={{

     }}>
        <TextInput
           style={{
                borderBottomWidth:1,
                borderColor:'#F4F6F7',
                width:'88%',
                position:'relative',
                marginLeft:40
           }}
        />
     </View>
     <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 20,
          borderColor: "#00716F",
          borderRadius: 23,
          width:'100%'
        }}
      >
        <Image
        source={require("../images/lockIcon.png")}
        style={{ width: 30, height: 30 }}
        />
         <Text
            style={{
            fontSize: 14,
            color:'#93A5B3',
            marginLeft:10
            }}
        >
           Contraseña
        </Text>
        <Text
            onPress={()=>navigation.navigate('Recuperar')}
            style={{
            fontSize: 14,
            textAlign: "right",
            color:'#5E798D',
            marginLeft:10,
            width:'65%'
            }}
        >
           Olvidaste?
        </Text>
     </View>
     <View style={{

     }}>
        <TextInput
           style={{
                borderBottomWidth:1,
                borderColor:'#F4F6F7',
                width:'88%',
                position:'relative',
                marginLeft:40
           }}
        />
     </View>
            <View style={{
                width:'100%',
                alignItems:"center",
                justifyContent:"center",
                marginTop:30,
                backgroundColor:"#0EEFE9",
                padding:12,
                borderRadius:12
                }}>
                    <Text
                     onPress={()=>navigation.navigate('Register')}
                    style={{
                        color:"#284C67",

                    }}>Ingresar</Text>
            </View>
            <Text
                style={{
                    fontSize:12,
                    textAlign:"center",
                    color:'#5E798D',
                    marginTop:20
                }}
                >
                O, ingresa con:
            </Text>
            <View  style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 20,
                borderColor: "#00716F",
                borderRadius: 23,
                width:'100%'
                }}>
            <SvgUri
                width="48%"
                height="50px"
                uri="https://www.softworkspy.com/facebook.svg"
              />
            <SvgUri
                width="48%"
                height="50px"
                uri="https://www.softworkspy.com/google.svg"
                style={{
                  marginLeft:"4%"
              }}
              />
           
            </View>
            <Text
                style={{
                    fontSize:12,
                    textAlign:"center",
                    color:'#5E798D',
                    marginTop:30,
                }}
                >
                No tienes una cuenta? <Text
                                    onPress={()=>navigation.navigate('Register')}
                                    style={{
                                        fontSize:12,

                                        color:'#284C67',
                                        textDecorationLine:'underline',
                                    }}
                                    >
                                     Registrate aquí
                                </Text>
            </Text>
    </View>
  </View>
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
   },
   highlight: {
   },
 });
 
 export default App;
 