/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import {
   StyleSheet,
   Text,
   View,
   Image
 } from 'react-native';
 
 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 
 
 
 const App: () => Node = ({ navigation }) => {

   return (
    <View style={{backgroundColor:"#FFF",height:"100%"}}>
    <Image source ={require('../images/intro-bg.png')}
        style={{width:"100%",height:"60%"}}
    />
     <View style={{
         backgroundColor:"#FFF",
         paddingLeft:20,
         paddingRight:20
         }}>
        <Text
        style={{
            fontSize:16,

            color:'#284C67',
            
        }}
        >Para profecionales de</Text>

        <Text
        style={{
            fontSize:25,

            color:'#284C67'
        }}
        >
          Belleza y Wellnes
        </Text>
        
        <Text
        style={{
            fontSize:12,

            color:'#284C67',
            marginTop:20
        }}
        >
         La herramienta que ayuda a organizar citas
            y lleva los ingresos de manera fácil y amigable.
        </Text>

        <View style={{
        width:'100%',
        alignItems:"center",
        justifyContent:"center",
        marginTop:30,
        backgroundColor:"#0EEFE9",
        padding:12,
        borderRadius:12
        }}>
            <Text
             onPress={()=>navigation.navigate('Register')}
            style={{
                color:"#284C67",

            }}>Registrarme</Text>
        </View>
        <View style={{
        width:'100%',
        alignItems:"center",
        justifyContent:"center",
        marginTop:30,
        backgroundColor:"#F4F6F7",
        padding:12,
        borderRadius:12
        }}>
            <Text
             onPress={()=>navigation.navigate('Login')}
             style={{
                color:"#00716F",

            }}>Iniciar sesión</Text>
        </View>
    </View>
</View>
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
 });
 
 export default App;
 